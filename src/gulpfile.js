var 
  gulp            = require('gulp'),
  gutil           = require('gulp-util'),
  bower           = require('bower'),
  concat          = require('gulp-concat'),
  header          = require('gulp-header'),
  minifyCss       = require('gulp-minify-css'),
  rename          = require('gulp-rename'),
  sh              = require('shelljs'),
  less            = require('gulp-less'),
  uglify          = require('gulp-uglify'),
  sourcemaps      = require('gulp-sourcemaps'),
  livereload      = require('gulp-livereload')
  ;

var 
  paths = {
    less:         ['less/**/*.less'],
    scripts:      ['js/**/*.js']
  },
  dest    = './../',
  banner  = '/* Generated <%= (new Date()).toUTCString() %> */\n';

gulp.task('css', function(done) {
  
  // vendor css

  gulp
    .src([
    ])
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest(dest + 'assets/css'));
    
  // main css
  
  return gulp
    .src([
      'less/main.less'
    ])
    .pipe(less())
    .pipe(rename('main.css'))
    .pipe(gulp.dest(dest + 'assets/css'))
    .pipe(livereload());
    
        
});

gulp.task('scripts', function() {

  // vendor js
  
  gulp.
    src([
    ])
    .pipe(concat('vendor.js')).on('error', errorHandler)
    .pipe(header(banner))
    .pipe(gulp.dest(dest + 'assets/js'));
    
  // app js

  return gulp
    .src([
      'js/main.js'
    ])
    .pipe(concat('main.js')).on('error', errorHandler)
    .pipe(header(banner))
    .pipe(gulp.dest(dest + 'assets/js'))
    .pipe(livereload()).on('error', errorHandler);
  
});

gulp.task('watch', function() {
  
  livereload.listen();

  gulp.watch('gulpfile.js', [ 'css', 'scripts' ]);
  gulp.watch(paths.less, ['css']);
  gulp.watch(paths.scripts, ['scripts']);
      
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});

function errorHandler (error) {
  console.log(error.toString());
  this.emit('end');
}
